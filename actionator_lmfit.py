import actionatorlib as al
import scipy as spy
from scipy import optimize
from time import time as real_time
import numpy as np
from hooke_jeeves_bounded import hooke
from lmfit import minimize, Parameters

from pprint import pprint as pp

def fit_to_bates_data():
    model = al.read_model("full_model.mo")

    model.add_observable("TAG","TAG_1")
    model.add_observable("DAG","DAG_1 + DAG_2 + DAG_3")
    model.add_observable("PC","PC_1 + PC_2")
    
    #data_set = al.Dataset("data/Arabidopsis_glycerol_bates_with_100day_projection.txt") #should be pre-normalized and pre-shifted etc. (although these functions could be added later)
    data_set = al.Dataset("data/Arabidopsis_glycerol_bates.txt")
    
    model_fit = al.Fit()
    model_fit.add_pair(model, data_set)
    
    #rate_consts = {"k0": 80.548,"k1": 0.074827,"k2": 0.83183,"k3": 1.0,"k4": 0.2375,"k5": 0.013433,"k6": 0.228894,"k7": 0.233858,"k8": 1.20513,"k9": 0.058923,"k10": 0.291628} # a not good guess
    rate_consts = {'k10': 0.41862042089569257, 'k3': 0.80981489011802754, 'k2': 1.2892659974975955, 'k1': 0.15885991275918065, 'k0': 80.684565387524657, 'k7': 0.26521563167642959, 'k6': 0.28381963753586231, 'k5': 0.0014838932677796639, 'k4': 0.14089117100713272, 'k9': 0.0082431331456686886, 'k8': 1.2470092848644059}
    #rate_consts = {x:1 for x in rate_consts}
    params = Parameters()
    for (k, v) in rate_consts.iteritems():
        params.add(k, value=1, min=0.0)
    
    (guess_order, guess) = al.dict_to_lists(rate_consts)
    print guess_order
    #bounds = [(0,None) for x in guess]
    
    #result_structure = optimize.minimize(model_fit.fit, guess, bounds=bounds, args=(guess_order,), method='L-BFGS-B', options={'maxiter': 600, 'ftol':0.0000000000001})
    result_structure = minimize(model_fit.lmfit, params)
    print result_structure
    
    #result = al.list_to_dict(result_structure.x , guess_order)
    #print result
    
    
    # model.set_rate_constants(result)
    
    # #times = np.linspace(0,max(data_set.times()),100)
    # #times = np.linspace(0,100,100)
    # times = data_set.times()
    # (obs, met) = model.integrate(times)
    # pprint.pprint(obs)
    # pprint.pprint(data_set.data())
    # al.graph_solution(obs, times)

def main():
    model = al.read_model("full_model.mo")
    #TODO: there should be a way to define observables in the model file if desired
    model.add_observable("TAG","TAG_1")
    model.add_observable("DAG","DAG_1 + DAG_2 + DAG_3")
    model.add_observable("PC","PC_1 + PC_2")
    
    #model2 is just like model except that at time 15, k0 becomes stuck at 0
    model2 = model.copy()
    model2.add_observable("TAG_p","100 * TAG_1/(TAG_1 + DAG_1 + DAG_2 + DAG_3 + PC_1 + PC_2 + 0.0000000001)") #0.0000000001 is just a tiny number to avoid divide by zero errors
    model2.add_observable("DAG_p","100 * (DAG_1 + DAG_2 + DAG_3)/(TAG_1 + DAG_1 + DAG_2 + DAG_3 + PC_1 + PC_2 + 0.0000000001)")
    model2.add_observable("PC_p","100 * (PC_1 + PC_2)/(TAG_1 + DAG_1 + DAG_2 + DAG_3 + PC_1 + PC_2 + 0.0000000001)")
    model2.add_trigger(15,'k0',0) #this is in the interpretter, not in the model file, because it is not a property of the system, but a property of the experiment
    
    
    data_set1 = al.Dataset("data/Arabidopsis_glycerol_bates.txt") #should be pre-normalized and pre-shifted etc. (although these functions could be added later)
    data_set2 = al.Dataset("data/Arabidopsis_glycerol_Lu_chase_normalized.txt")# TODO: MAKE SURE THIS DATA HAS _p IN THE DATA LABELS!!!!
    
    model_fit = al.Fit()
    
    model_fit.add_pair(model, data_set1)
    model_fit.add_pair(model2, data_set2)
    
    sorted_rate_constants = [x for x in model.rate_constants]
    guess = {"k0": 143.548,"k1": 0.074827,"k2": 0.53183,"k3": 0.0,"k4": 0.2375,"k5": 0.013433,"k6": 0.228894,"k7": 0.233858,"k8": 1.20513,"k9": 0.058923,"k10": 0.291628} # a pretty good guess
    #guess = {"k0": 80.548,"k1": 0.074827,"k2": 0.83183,"k3": 1.0,"k4": 0.2375,"k5": 0.013433,"k6": 0.228894,"k7": 0.233858,"k8": 1.20513,"k9": 0.058923,"k10": 0.291628} # a significantly worse guess
    #initial_guess = [guess[x] for x in sorted_rate_constants]
    #initial_guess = [0.11569769182583439, 0.2376290125567016, 0.6482764446921303, 0.07452676543453572, 143.7128656874192, 0.22967212612656457, 0.12287337351990732, 0.002968408676831098,0.065424755344191, 0.004584425320272999, 0.15634175609956]
    #bounds = [(0,None) for x in sorted_rate_constants] #COBYLA apparently ignores these and tries negative negative numbers anyways
    #bounds = [(0,100000000) for x in sorted_rate_constants] #for some reason, SLSQP doesn't like "None" as an upper constraint
    #result_structure = optimize.minimize(model_fit.fit, initial_guess, bounds=bounds, args=(sorted_rate_constants,), method='L-BFGS-B', options={'maxiter':1}, tol=1) 
    #result_structure = hooke(model_fit.fit, initial_guess, bounds=bounds, args=(sorted_rate_constants,), rho=0.2, epsilon=1E-5) #1E-10 seems to be too big 
    #print result_structure
    #result = al.list_to_dict(result_structure.x , sorted_rate_constants)
    #print result
    
    params = Parameters()
    for (k, v) in guess.iteritems():
        params.add(k, value=1, min=0.0)
    
    #(guess_order, guess) = al.dict_to_lists(rate_consts)
    #print guess_order
    #bounds = [(0,None) for x in guess]
    
    #result_structure = optimize.minimize(model_fit.fit, guess, bounds=bounds, args=(guess_order,), method='L-BFGS-B', options={'maxiter': 600, 'ftol':0.0000000000001})
    result_structure = minimize(model_fit.lmfit, params)
    print result_structure
    
    
    
    # model.set_rate_constants(result)
    # #model.set_rate_constants(guess)
    # #times = np.linspace(0,model_fit.max_time(),100)
    # times = np.linspace(0,max(data_set1.times()),100)
    # (obs, met) = model.integrate(times)
    
    # al.graph_solution(obs, times)
    #graph_solution(data_set1.data(),data_set1.times())
    
    
    
    #start = real_time()
    #Result = model_fit.fit(initial_guess, sorted_rate_constants)
    #print "fit time: " + str(real_time() - start)

    
    #data_set1.graph()
    #model.graph(Result.parameters) #this will be more complicated than that...
    
    #models = al.read_metamodel("full_model.mm")
    #Results = list()
    #for model in models:
        #Results.append(spy.optimize.minimize(model_fit.f, initial_guess))
        




    
if __name__ == '__main__':
    #fit_to_bates_data()
    main()