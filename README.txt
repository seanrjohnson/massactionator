MassActionator is a python library for simulating kinetic metabolic models.

The project focuses on simultaneous parameter fitting of alternative models to multiple sets of experimental data.

There are two kinds of input files, models and metamodels:

a model file (.mo) describes a metabolic model

there are two kinds of lines, constant lines and reaction lines
constant lines are of the form
[identifier] = [string]
wherever an identifer defined as a constant occurs in a reaction equation columns 
or a rate expression column, it will be replaced by the right-hand side string of constant
expression before being parsed
constants can be defined anywhere in the script and affect reactions throughout the script,
so it is suggested that the all be defined at the top of the script, for clarity.

the '#' symbol indicates that the remaining portions of a line are a comment and should be ignored

reaction lines consist of three tab-separated fields
field 1 is the reaction name, which can be any valid string (not containing a '#' or a tab, or an @ symbol)
field 2 is the reaction stoichiometry which is in the form of a typically written reaction equation:

<reaction> ::= <half_reaction> "=" <half_reaction>
<half_reaction> ::= <identifier> | <number> <indentifier> | <half_reaction> "+" <half_reaction>
<identifier> ::= <a-zA-Z_> | <identifier> <a-zA-Z_0-9>
<number> is anything that can be cast to a python float 
whitespace optional, but highly recommended for clarity and legibility

field 3 is the rate expression, it can contain any identifiers that were used in reaction stoichiometries
in addition, it can use "time" as an identifier. Other identifiers will be interpretted as rate constants
(i.e. independent variables)

a metamodel file (.mm) describes a model and various permutations of that model

the metamodel format is a superset of the model format
In addition to the model syntax, it has metamodeling directives
these begin with an @ symbol
parts of the metamodel to be included in only some realized models are encolsed in a
@<identifier>{...}
structure
@ directives with no curly braces describe how the parts should be combined to produce complete models



for similar (and far further developed) tools, see:
http://pysces.sourceforge.net/
http://mudshark.brookes.ac.uk/ScrumPy
http://modelmage.org/
http://www.copasi.org (particularly the API)