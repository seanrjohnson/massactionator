@0  #all
@(A)
@(A,B)
@combinations(A,B,C,D) #all possible combinations of one or more of the listed tags
glycerol uptake	= glycerol	k0
denovo DAG synthesis	glycerol = DAG_1	k1*glycerol
DAG_1 to PC_1	DAG_1 = PC_1	k2*DAG_1
@A{PC_1 to DAG_1	PC_1 = DAG_1	k3*PC_1}
PC_1 to PC_2	PC_1 = PC_2	k4*PC_1
PC_2 to PC_1	PC_2 = PC_1	k5*PC_2
PC_1 to DAG_2	PC_1 = DAG_2	k6*PC_1
@B{DAG_2 to PC_1	DAG_2 = PC_1	k7*DAG_2}
DAG_2 to DAG_3	DAG_2 = DAG_3	k8*DAG_2
DAG_3 to DAG_2	DAG_3 = DAG_2	k9*DAG_3
DAG_2 to TAG	DAG_2 = TAG	k10*DAG_2
@C{DAG_2 to PC_2	DAG_2 = PC_2	k11*DAG_2}
@C{PC_2 to DAG_2	PC_2 = DAG_2	k12*PC_2}
@D{DAG_1 to TAG	DAG_1 = TAG	k13*DAG_1}