import sys
sys.path.append("../..")
from unittest import TestCase
import unittest
import massactionator.actionatorlib as al
from massactionator.test import __file__
from StringIO import StringIO
import os

# from
#http://techscapades.com/blog/2009/01/13/including-external-data-in-python-unit-tests/
def data_dir():
    return os.path.join(os.path.dirname(__file__), 'data')

# class test_read_model(TestCase):
    # def setUp(self):
        # #runs before each test case
        # self.model = al.read_model(os.path.join(data_dir(),"full_model.mo"))
        # self.model.add_observable("TAG","TAG_1")
        # self.model.add_observable("DAG","DAG_1 + DAG_2 + DAG_3")
        # self.model.add_observable("PC","PC_1 + PC_2")
        # self.model.add_trigger(15,'k0',0)
        # self.model.add_trigger(20,'TAG_1',0)
    # def test_null(self):
        # #print self.model.reactions
        # # for (name, reaction) in self.model.reactions.items():
            # # print name
            # # print reaction.lhs
            # # print reaction.rhs
            # # print reaction.rate_law
        
        # # for (name, expression) in self.model.diffeqs.items():
            # # print ""
            # # print name
            # # print expression
            # # print type(expression)
        
        # self.model.rate_constants = {x : 1 for x in self.model.rate_constants}
        # solution = self.model.integrate([1,3,10,15,17,20,25,30])
        # print solution
class test_dataset(TestCase):
    def setUp(self):
        self.dataset = al.Dataset(os.path.join(data_dir(),"test_dataset.tsv"))
    def test_read(self):
        times = [0.,5.,10.,15.]
        observables = set(["DAG","PC","TAG"])
        self.assertEqual(observables, self.dataset.observables())
        self.assertEqual(times, self.dataset.times())
        self.assertEqual(0., self.dataset.get("TAG",0))
        self.assertEqual(5.3, self.dataset.get("PC",15))
        self.assertEqual(1.2, self.dataset.get("DAG",5))


if __name__ == '__main__':
    unittest.main()