#
#   A library to read a biochemical model from a text file and convert
#   it into a set of differential equations, and then fit the model parameters
#   to experimental data
#

from sympy import S, Symbol
import sympy as sy
import re
import copy
import scipy as spy
from scipy import integrate
import operator
import numpy as np
from time import time as real_time
import csv
import matplotlib.pyplot as plt
from pprint import pprint as pp

EQUALS_SIGNS = {'=': 'undefined'}#, '<=>': 'bidirectional', '=>': 'left to right', '<=': 'right to left'}
IDENTIFIER_REGEX = '[A-Za-z_][A-Za-z_0-9]*'
sympy_time = 0.
other_time = 0.
its = 0

class Trigger:
    def __init__(self, time, parameter, value):
        ### a parameter can be either a substrate concentration or a k value ###
        ### a value must be a numerical constant ###
        self.time = time
        self.parameter = parameter
        self.value = value


class Model(object):
    def __init__(self, reactions, rate_constants, initial_conditions = dict(), observables = dict()): 
        #'time' could also be a symbol, but it's special
        #not sure quite where the best place to document that is
        self.reactions = reactions #dict, keys are reaction names, values are Reaction objects
        #print "rate constants"
        #print rate_constants
        self.rate_constants = {x:0. for x in rate_constants} # dict values are floats, keys are rate_constant names
        self.substrates = self._recalculate_substrates() #set of strings
        
        ### set triggers ###
        self.triggers = list()
        
        ### set observables ###
        self.observables = dict() #keys are names, values are expressions (in terms of substrates)
        for observable in observables:
            self.add_observable(observable, observables[observable])
        
        ### set initial conditions ###
        self.initial_conditions = dict() #keys are substrate names, values are floats
        for substrate in initial_conditions:
            if substrate in self.substrates:
                self.initial_conditions[substrate] = initial_conditions[substrate]
        # assume zeros if no initial conditions are assigned
        uninitialized_substrates = self.substrates.difference(set(self.initial_conditions.keys()))
        for substrate in uninitialized_substrates:
            self.initial_conditions[substrate] = 0.
        
        self.diffeqs_full = self._calculate_full_diffeqs() #dict where keys are substrate names and values are sympy expressions (substrate concentrations as "substrate(time)", derivatives as "derivative(substrate(time),time) )
        self.diffeqs = self._calculate_diffeqs() #dict where keys are substrate names and values are 
        self.diffeqs_strings = {x:str(self.diffeqs[x]) for x in self.diffeqs}
        self.diffeqs_compiled = {x:compile(str(self.diffeqs[x]),'<string>', 'eval') for x in self.diffeqs}
    
    
    def _recalculate_substrates(self):
        out = set()
        for rxn in self.reactions.values():
            out = out.union(rxn.all_substrates())
        return out
    
    def set_rate_constants(self, rate_constants):
        """
            rate_constants is a dict. Keys are strings of rate constant names, values are numbers
            
        """
        for c in rate_constants:
            if c in self.rate_constants:
                self.rate_constants[c] = rate_constants[c]
    
    def copy(self):
        return copy.deepcopy(self)
    
    def _calculate_full_diffeqs(self):
        """
            calculate the differential equations for the change in concentration
            of each substrate over time.
            
            return a dict where keys are substrate names and values are sympy expressions of the form
                [0 =] sum(rate_laws) - dSubstrate/dTime
        """
        diffeqs = dict()
        substrate_functions = dict()
        substrate_derivatives = dict()
        time = Symbol('time')
        for substrate in self.substrates:
            substrate_functions[substrate] = sy.Function(substrate)(time) #substrate concentration is a function of time
            substrate_derivatives[substrate] = sy.Derivative(substrate_functions[substrate], time) #make a variable for the derivative
        
        for substrate in self.substrates:
            diffeqs[substrate] = -substrate_derivatives[substrate]
            for reaction in self.reactions.values():
                minus_coeff = None
                plus_coeff = None
                minus_exp = None
                plus_exp = None
                if substrate in reaction.lhs:
                    minus_coeff = -1 * reaction.lhs[substrate]
                    minus_exp = reaction.rate_law.subs(substrate_functions)
                if substrate in reaction.rhs:
                    plus_coeff = reaction.rhs[substrate]
                    plus_exp = reaction.rate_law.subs(substrate_functions)
                if not minus_coeff is None:
                    diffeqs[substrate] += minus_coeff * minus_exp
                if not plus_coeff is None:
                    diffeqs[substrate] += plus_coeff * plus_exp
        return diffeqs

    def _calculate_diffeqs(self):
        """
            calculate the differential equations for the change in concentration
            of each substrate over time.
            
            return a dict where keys are substrate names and values are sympy expressions of the form
                [dSubstrate/dTime =] sum(rate_laws)
        """
        diffeqs = dict()
        #substrate_functions = dict()
        #substrate_derivatives = dict()
        #time = Symbol('time')
        #for substrate in self.substrates:
        #    substrate_functions[substrate] = sy.Function(substrate)(time) #substrate concentration is a function of time
        #    substrate_derivatives[substrate] = sy.Derivative(substrate_functions[substrate], time) #make a variable for the derivative
        
        for substrate in self.substrates:
            diffeqs[substrate] = 0 #-substrate_derivatives[substrate]
            for reaction in self.reactions.values():
                minus_coeff = None
                plus_coeff = None
                minus_exp = None
                plus_exp = None
                if substrate in reaction.lhs:
                    minus_coeff = -1 * reaction.lhs[substrate]
                    minus_exp = reaction.rate_law#.subs(substrate_functions)
                if substrate in reaction.rhs:
                    plus_coeff = reaction.rhs[substrate]
                    plus_exp = reaction.rate_law#.subs(substrate_functions)
                if not minus_coeff is None:
                    diffeqs[substrate] += minus_coeff * minus_exp
                if not plus_coeff is None:
                    diffeqs[substrate] += plus_coeff * plus_exp
        return diffeqs


##### This version for "diffeqs_full" #####
    # def exact_derivatives(self, concentrations, time, k_values, substrate_order):
        # #seg_sol = spy.integrate.odeint(self.exact_derivative, initial_conditions, segment, args=(k_values, sorted_constants, sorted_substrates))
        # global its
        # global other_time
        # global sympy_time
        # its += 1
        # start_time = real_time()
        # sub_subs = dict()
        # out = [0] * len(substrate_order)
        # for s_i in range(len(substrate_order)):
            # sub_subs[function_string(substrate_order[s_i], "time")] = concentrations[s_i]
        # other_time += real_time() - start_time
        
        # start_time = real_time()
        # for s_i in range(len(substrate_order)):
            # st = real_time()
            # out[s_i] = sy.solve(self.diffeqs_full[substrate_order[s_i]], derivative_string(substrate_order[s_i],"time"))[0]
            # print "1: " + str(real_time() - st)
            # #print out[s_i]
            # st = real_time()
            # out[s_i] = out[s_i].subs(sub_subs)
            # print "2: " + str(real_time() - st)
            # #print out[s_i]
            # st = real_time()
            # out[s_i] = out[s_i].subs(k_values) 
            # print "3: " + str(real_time() - st)
            # #print out[s_i]
            # #print sy.solve(out[s_i], derivative_string(substrate_order[s_i],"time"))
            # #out[s_i] = sy.solve(out[s_i], derivative_string(substrate_order[s_i],"time"))
        # sympy_time += real_time() - start_time
        # return out

    def exact_derivatives(self, concentrations, time, k_values, substrate_order):
        #seg_sol = spy.integrate.odeint(self.exact_derivative, initial_conditions, segment, args=(k_values, sorted_constants, sorted_substrates))
        global its
        global other_time
        global sympy_time
        its += 1
        start_time = real_time()
        sub_subs = dict()
        out = [0] * len(substrate_order)
        #print concentrations
        #print k_values
        for s_i in range(len(substrate_order)):
            sub_subs[substrate_order[s_i]] = concentrations[s_i]
        sub_subs['time'] = time
        #print sub_subs
        #print k_values
        for (key, val) in k_values.items():
            sub_subs[key] = val
        other_time += real_time() - start_time
        
        
        start_time = real_time()
        #print sub_subs
        for s_i in range(len(substrate_order)):
            #st = real_time()
            #print self.diffeqs[substrate_order[s_i]]
            
            out[s_i] = self.diffeqs[substrate_order[s_i]].subs(sub_subs) #slow
            #out[s_i] = self.diffeqs[substrate_order[s_i]].evalf(subs=sub_subs) #even slower
            #print str(its) +": " + str(real_time() - st)
            
            #print out[s_i]
            #st = real_time()
            #out[s_i] = out[s_i].subs(k_values) 
            #print "3: " + str(real_time() - st)
            #print out[s_i]
            #print sy.solve(out[s_i], derivative_string(substrate_order[s_i],"time"))
            #out[s_i] = sy.solve(out[s_i], derivative_string(substrate_order[s_i],"time"))
        sympy_time += real_time() - start_time

        return out
    # def exact_derivatives_string(self, concentrations, time, k_values, substrate_order):
        # #seg_sol = spy.integrate.odeint(self.exact_derivative, initial_conditions, segment, args=(k_values, sorted_constants, sorted_substrates))
        # global its
        # global other_time
        # global sympy_time
        # its += 1
        # #start_time = real_time()
        # sub_subs = dict()
        # out = [0] * len(substrate_order)
        # #print concentrations
        # #print k_values
        # for s_i in range(len(substrate_order)):
            # sub_subs[substrate_order[s_i]] = concentrations[s_i]
        # sub_subs['time'] = time
        # #print sub_subs
        # #print k_values
        # for (key, val) in k_values.items():
            # sub_subs[key] = val
        # #other_time += real_time() - start_time
        
        
        # start_time = real_time()
        # #print sub_subs
        # for s_i in range(len(substrate_order)):
            # #st = real_time()
            # #print self.diffeqs[substrate_order[s_i]]
            
            # out[s_i] = eval(self.diffeqs_strings[substrate_order[s_i]], sub_subs) #TODO: make sure this eval is secure (I think the parsing as a sympy expression and recasting to a string that has been done should be enough, but this matter should be looked into further)
            # #print "2: " + str(real_time() - st)
            
            # #print out[s_i]
            # #st = real_time()
            # #out[s_i] = out[s_i].subs(k_values) 
            # #print "3: " + str(real_time() - st)
            # #print out[s_i]
            # #print sy.solve(out[s_i], derivative_string(substrate_order[s_i],"time"))
            # #out[s_i] = sy.solve(out[s_i], derivative_string(substrate_order[s_i],"time"))
        # sympy_time += real_time() - start_time

        # return out
    def exact_derivatives_compiled(self, concentrations, time, k_values, substrate_order):
        #seg_sol = spy.integrate.odeint(self.exact_derivative, initial_conditions, segment, args=(k_values, sorted_constants, sorted_substrates))
        global its
        global other_time
        global sympy_time
        its += 1
        #start_time = real_time()
        sub_subs = dict()
        out = [0] * len(substrate_order)
        #print concentrations
        #print k_values
        for s_i in range(len(substrate_order)):
            sub_subs[substrate_order[s_i]] = concentrations[s_i]
        sub_subs['time'] = time
        #print sub_subs
        #print k_values
        for (key, val) in k_values.items():
            sub_subs[key] = val
        #other_time += real_time() - start_time
        
        
        start_time = real_time()
        #print(sub_subs)
        for s_i in range(len(substrate_order)):
            #st = real_time()
            #print(self.diffeqs[substrate_order[s_i]])
            
            out[s_i] = eval(self.diffeqs_compiled[substrate_order[s_i]], sub_subs) #TODO: make sure this eval is secure (I think the parsing as a sympy expression and recasting to a string that has been done should be enough, but this matter should be looked into further)
            #print "2: " + str(real_time() - st)
            
            #print out[s_i]
            #st = real_time()
            #out[s_i] = out[s_i].subs(k_values) 
            #print "3: " + str(real_time() - st)
            #print out[s_i]
            #print sy.solve(out[s_i], derivative_string(substrate_order[s_i],"time"))
            #out[s_i] = sy.solve(out[s_i], derivative_string(substrate_order[s_i],"time"))
        sympy_time += real_time() - start_time

        return out
    
    def add_observable(self, name, expression):
        exp = S(expression)
        self.observables[name] = compile(str(exp), '<string>', 'eval')
    
    def add_trigger(self, time, parameter, value):
        if time >= 0:
            self.triggers.append(Trigger(time, parameter, value))
            #self.triggers = sorted(self.triggers)
        else:
            print "cannot add trigger at negative time"
    
    
    def integrate(self, time_points):
        """
            using whatever rate constants and initial conditions have been set for the model, integrate.
            
            time 0 is implicitly the first time point and negative time points are ignored
            
            If there are any triggers set, integrate to the trigger, make the change, then integrate the next
            segment, etc.
            
            outputs the concentrations of the observables and of the metabolites at all input time points.
            (observables, metabolites)
            observables is a dict of arrays, metabolites is a dict of arrays
        """
        #sort triggers and time points lowest to highest
        trig_times = set([x.time for x in self.triggers])
        sample_times = set(time_points)
        if 0 not in sample_times:
            sample_times.add(0)
        all_times = list(sample_times.union(trig_times))
        all_times = sorted(all_times)
        
        trig_times = threshold_set(trig_times,max(sample_times)) #remove trigger times that are out of the range of the sample times
        
        
        #sorted_constants = sorted(self.rate_constants.keys()) # a sorted list of rate constants
        sorted_substrates = sorted(self.substrates) # a sorted list of rate substrates
        
        #divide the time points into segments based on their relation to the triggers
        #the last point in all segments but the final segment should be a trigger point
        segments = list() # a list of lists, each sublist (except perhaps the last one) should end at a trigger
        #trig_idx = 0
        seg_index = 0
        segments.append(list())
        for pt in all_times:
            if pt in trig_times: #
                segments[seg_index].append(pt)
                seg_index += 1
                segments.append(list())
                segments[seg_index].append(pt) #segments should overlap by exactly one point
            else:
                segments[seg_index].append(pt)
        
        #remove the final segment if has only one point
        if len(segments[seg_index]) == 1:
            segments.pop()

        #integrate over each segment
        initial_conditions = self.initial_conditions
        sol = np.zeros([len(all_times), len(sorted_substrates)]) #an array for the integration results
        last_seg = 0
        for segment in segments:
            #(self, concentrations, time, k_values, k_order, substrate_order)
            k_values = self.calculate_k_values(segment[0]) #k_values are independent variables, so they will be only a function of time and triggers
            #print segment
            #print k_values
            #print initial_conditions
            initial_conditions = self.calculate_initial_conditions(initial_conditions, segment[0]) #if any triggers at time segment[0] change substrate concentrations, apply that
            #print initial_conditions
            sorted_initial_conditions = sort_dict(initial_conditions, sorted_substrates)
            #seg_sol = spy.integrate.odeint(self.exact_derivative, initial_conditions, segment, args=(k_values, sorted_constants, sorted_substrates))
            #start_time = real_time()
            #seg_sol = integrate.odeint(self.exact_derivatives, sorted_initial_conditions, segment, args=(k_values, sorted_substrates))
            #seg_sol = integrate.odeint(self.exact_derivatives_string, sorted_initial_conditions, segment, args=(k_values, sorted_substrates))
            seg_sol = integrate.odeint(self.exact_derivatives_compiled, sorted_initial_conditions, segment, args=(k_values, sorted_substrates))
            #print real_time() - start_time
            #print "iterations: " + str(its)
            #print "other: " + str(other_time)
            #print "sympy: " + str(sympy_time)
            #print seg_sol
            #print sol
            sol[last_seg:last_seg+seg_sol.shape[0],:] = seg_sol
            last_seg = last_seg + seg_sol.shape[0]-1 #minus 1 because the segments overlap by 1, and we want the second time a timepoint comes up to take priority
            initial_conditions = list_to_dict(sol[last_seg,:],sorted_substrates)
        
        out_metabolites = {x:list() for x in self.substrates}
        out_observables = {x:list() for x in self.observables}
        
        #global  sympy_time
        #calculate output
        for pt in range(len(time_points)):
            pt_index = all_times.index(time_points[pt])
            obs_subs = dict()
            for met in out_metabolites:
                met_index = sorted_substrates.index(met)
                out_metabolites[met].append(sol[pt_index, met_index])
                obs_subs[met] = sol[pt_index, met_index]
            
            #start = real_time()
            for obs in out_observables:
                #out_observables[obs].append(self.observables[obs].subs(obs_subs))
                #out_observables[obs].append(eval(str(self.observables[obs]), obs_subs))
                out_observables[obs].append(eval(self.observables[obs], obs_subs))
            #sympy_time += real_time() - start
        return (out_observables, out_metabolites)
    
    def calculate_initial_conditions(self, initial_conditions, time):
        out = initial_conditions
        for trigger in self.triggers:
            if trigger.time == time:
                if trigger.parameter in initial_conditions:
                    out[trigger.parameter] = trigger.value
        return out
    
    def calculate_k_values(self, time):
        out = self.rate_constants
        for trigger in self.triggers:
            if trigger.time <= time:
                if trigger.parameter in self.rate_constants:
                    out[trigger.parameter] = trigger.value
        return out

# def func_from_eval(string):
    # """
        # takes a string of python code and returns a function such that the code in the string is eval'ed ("compiled") only once
        # but can be executed many times.
        
        # The idea is to speed up processes that are limited by the slowness of eval.
    # """
    # string = string.strip()
    # string = "def func(vars):\n\t" + string
##### nevermind... compile seems to do a good enough job, probably at least as good as this would do
    
    
    
def sort_dict(d, order):
    """
        d is a dict
        order is a list where values are keys of d
        
        returns a list where indexes are the same as in "order" but values are the values of d rather than the keys
    """
    
    out = [d[x] for x in order]
    return out

def list_to_dict(in_list, order):
    """
        input: in_list members are to be used as values of the output dict, order members are to be used as keys, for the data in the corresponding position in in_list
        output: a dict with in_list members as values and order members as keys
        
        (basically, it's taking a n by 2 array (each input vector is a column) and using one column as the keys and the other as the values for a dict
        if entries in order are not unique, then not all values in in_list will be in the output (as implemented, only the one with the larger index will be there)
    """
    out = dict()
    for s_i in range(len(order)):
        out[order[s_i]] = in_list[s_i]
    return out

def dict_to_lists(in_dict):
    """
        input: a dict
        output: two lists, one is the keys of dict, the other the values. such that the same position in each list corresponds
        
        This is the inverse operation of list_to_dict
    """
    keys = sorted(in_dict.keys())
    vals = [in_dict[x] for x in keys]
    
    return (keys, vals)
    
class SteadyStateMap(Model):
    #def __init__(self, reactions, rate_constants, initial_conditions = dict(), observables = dict(), source_model = None, parameter_map = dict(), steady_state_time_estimation=1e6): 
    def __init__(self, template_model, source_model, parameter_map = dict(), steady_state_time_estimation=1e6): 
        '''
            parameter map is of the form: parameter_from_destination_model: equation_using_parameters_and_species_from_model1
        '''
        template_copy = template_model.copy()
        for (k,v) in vars(template_copy).items():
            setattr(self, k, v)
        
        self.source = source_model.copy()
        self.map = dict()
        for (k,v) in parameter_map.items():
            self.map[k] = str(S(v))
        self.end_point = steady_state_time_estimation
        #super(SteadyStateMap,self).__init__(reactions, rate_constants, initial_conditions, observables)
    
    def set_rate_constants(self, rate_constants):
        """
            rate_constants is a dict. Keys are strings of rate constant names, values are numbers
        """

        input_constant_names = set(rate_constants.keys())
        parameter_map_names = set(self.map.keys())
        for c in rate_constants:
            if c in self.rate_constants:
                self.rate_constants[c] = rate_constants[c]
        
        if len(parameter_map_names.intersection(input_constant_names)) > 0:
            self.source.set_rate_constants(rate_constants)
            (observables, metabolites) = self.source.integrate([self.end_point])
            variables = dict()
            variables.update({k:v[-1] for (k,v) in metabolites.items()})
            variables.update({k:v[-1] for (k,v) in observables.items()})
            variables.update(self.rate_constants)
            #pp(variables)
            #pp(self.map)
            for k in self.map:
                self.rate_constants[k] = eval(self.map[k], variables)
            #pp(self.rate_constants)

    
        
        
class Fit():
    """
        Calculates goodness of fit between one or more models and one or more datasets
    """
    def __init__(self, method='sum_of_squares', minimax=False):
        self.method = method #TODO: implement this
        self.minimax = minimax #TODO: implement this
        self.pairs = list() #list of 2-tuples (Model, Dataset)
    
    def add_pair(self, model, dataset):
        self.pairs.append((model, dataset))
    
    def max_time(self):
        """
            returns the largest time point in any of the child datasets
        """
        
        for (model, dataset) in self.pairs:
            times = dataset.times()
        
        return max(times)
    
    def fit(self, rate_constants_list, sorted_rate_constants):
        """
            calculates goodness of fit between the models and the datasets when the given rate constants are used as initial conditions
        """
        pp(rate_constants_list)
        pp(sorted_rate_constants)
        rate_constants = list_to_dict(rate_constants_list, sorted_rate_constants)
        #print rate_constants
        goodness = 0
        
        for (model, dataset) in self.pairs:
            times = dataset.times()
            model.set_rate_constants(rate_constants)
            #start = real_time()
            (observables, all) = model.integrate(times)
            #print real_time() - start
            #print(observables)
            #print(dataset.data())
            tmp_goodness = sum_of_squares(dataset.data(), observables)
            #print tmp_goodness
            goodness += tmp_goodness
            #goodness += absolute_difference(dataset.data(), observables)
            #goodness += relative_difference_squares(dataset.data(), observables)
        print goodness
        #print "eval time: " + str(sympy_time)
        
        return goodness
    
    def lmfit(self, params):
        """
            calculates goodness of fit between the models and the datasets when the given rate constants are used as initial conditions
        """
        #rate_constants = {x:params[x].value for x in params}
        rate_constants = params
        #print rate_constants
        goodness = list()
        
        for (model, dataset) in self.pairs:
            times = dataset.times()
            model.set_rate_constants(rate_constants)
            #start = real_time()
            (observables, all) = model.integrate(times)
            #print real_time() - start
            goodness += residuals(dataset.data(), observables)
            #goodness += absolute_difference(dataset.data(), observables)
            #goodness += relative_difference_squares(dataset.data(), observables)
        #print goodness
        #print "eval time: " + str(sympy_time)
        sum_sq = 0
        for val in goodness:
            sum_sq += val**2
        print sum_sq
        
        return goodness
            
def absolute_difference(data, model):
    total_diff = 0
    for obs in data:
        for i in range(len(data[obs])):
            total_diff += abs(data[obs][i] - model[obs][i])
    return total_diff

def relative_difference(data, model):
    total_diff = 0
    for obs in data:
        for i in range(len(data[obs])):
            total_diff += abs((data[obs][i] - model[obs][i])/data[obs][i])
    return total_diff
def relative_difference_squares(data, model):
    total_diff = 0
    for obs in data:
        for i in range(len(data[obs])):
            total_diff += ((data[obs][i] - model[obs][i])/data[obs][i])**2
    return total_diff

def sum_of_squares(data, model):
    total = 0
    for obs in data:
        for i in range(len(data[obs])):
            total += (data[obs][i] - model[obs][i])**2
    return total

def residuals(data, model):
    res = list()
    for obs in data:
        for i in range(len(data[obs])):
            res.append(data[obs][i] - model[obs][i])
    return res
def graph_solution(solution, times):
    
    
    # with help from http://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    for (name, value) in solution.iteritems():
        ax.plot(times, value, label=name)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    
    ax.legend(loc='upper left', bbox_to_anchor=(1, 1))
    plt.show()
    plt.close()

def distinct_colors(num):
    """
        returns rgb hex values for num distinct colors
    """
    out = list()
    if num > 0:
        divs = np.linspace(0,(1./num)*(num-1.), num) #divide a unit circle (circumference of 1) into num subdivisions
        
        for point in divs:
            out.append(point_to_color(point))

    return out

def point_to_color(point):
    """
        return the hex color corresponding to the point
    """
    blue = 0x0000FF
    red = 0xFF0000
    green = 0x00FF00
    if point >= 1:
        point = 0
    circle_divisions = 1530
    num = int(point*1530)
    #print num
    
    if num <= 0xFF:
        out = blue + num*0x10000
    elif num <= 0x1FF:
        out = blue + red - (num - 0x100)*0x10000
    elif num <= 0x2FF:
        out = red + (num - 0x200)*0x100
    elif num <= 0x3FF:
        out = green + red - (num - 0x300)*0x100
    elif num <= 0x4FF:
        out = green + (num - 0x400)
    elif num <= 0x5FF:
        out = green + blue - (num - 0x400)
    out = num_to_hex_color(out)
    return out
    
def num_to_hex_color(num):
    num = num % 0x1000000 #if its bigger than that, cut it down
    string = hex(num)
    string = string[2:]
    if len(string) < 6:
        string = ("0" * (6 - len(string)) ) + string
    string = "#" + string
    return string

def graph_multiple(*sets):
    """
        each set is a tuple where the first member is a dict of arrays (keys are names, values are arrays of y values) and the second is an array (the time points, x-values). all arrays in the same tuple should be of the same length.  Arrays in different tuples can have different lengths
    """
    #count the number of lines to be plotted and find that many distinct colors
    line_count = 0
    for set_ in sets:
        line_count += len(set_[0])
    colors = distinct_colors(line_count)
    
    # with help from http://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    for set_ in sets:
        for (name, value) in set_[0].iteritems():
            # print name
            # print value
            # print set_[1]
            #ax.plot(set_[1], value, label=name, color=colors.pop())
            ax.plot(set_[1], value, label=name)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    
    ax.legend(loc='upper left', bbox_to_anchor=(1, 1))
    plt.show()
    plt.close()

class Dataset():
    """
        a set of experimental data
    """
    def __init__(self, string):
        self._times = dict() #keys are numbers (time points from the data), values are integers(the order of the time points in the metabolite lists)
        self._observables = dict() #keys are strings (metabolite names), values are lists of numbers (metabolite concentrations) position in the list corresponds to 
        with open(string, 'rb') as infile:
            tsv = csv.reader(infile, delimiter="\t")
            headings = tsv.next() #get the first row
            #print headings
            self._observables = {x:list() for x in headings[1:len(headings)]}
            line_num = 0
            for line in tsv:
                self._times[float(line[0])] = line_num
                line_num += 1
                for i in range(1,len(headings)):
                    self._observables[headings[i]].append(float(line[i]))
    def get(self, name, time):
        out = self._observables[name][self._times[time]]
        return out
    
    def times(self):
        out = sorted(self._times.keys())
        return out
    
    def observables(self):
        out = set(self._observables.keys())
        return out
    
    def data(self):
        """
            returns a dict where keys are observable names, and values are lists of datapoints in chronological order, corresponding to the times returned by Dataset.times
        """
        times = self.times()
        out = {x:list() for x in self._observables}
        for time in times:
            for obs in out:
                out[obs].append(self._observables[obs][self._times[time]])
        return out

class Reaction(object):
    def __init__(self, string):
        """
            a reaction object is generated from a reaction equation string
        """
        (left, right) = halve_string(string, EQUALS_SIGNS.keys())
        self.lhs = parse_reaction_half(left) #dict, keys: substrate names, values: stoichiometry
        self.rhs = parse_reaction_half(right) 
        
    def all_substrates(self):
        """
            returns a set of all substrates on any side of the reaction
        """
        out = set(self.left_substrates().union(self.right_substrates()))
        return out
    
    def left_substrates(self):
        return set(self.lhs.keys())
    
    def right_substrates(self):
        return set(self.rhs.keys())
    
    def add_rate_law(self, rate_law):
        self.rate_law = rate_law

def steady_state_map_from_model(model, source_model, parameter_map = dict(), steady_State_time_estimation=1e6):
    #source_model, parameter_map = dict(), steady_state_time_estimation=1e6
    #    def __init__(self, reactions, rate_constants, initial_conditions = dict(), observables = dict(), source_model, parameter_map = dict(), steady_state_time_estimation=1e6): 
    return SteadyStateMap(model, source_model, parameter_map, steady_State_time_estimation)
        
def read_model(filename): #infile):
    """
        filename: the path to a model file
    """
    # I'm torn between whether to accept a file handle rather or a file name here. I think it forces an unnecessary step on the calling script (that and as far as I can tell, no high quality libraries do it like this).
    
    reserved_identifiers = set(['time'])
    consts = dict()
    substrate_names = set()
    reactions = dict()
    rate_constants = set()
    
    
    
    
    used_identifiers = set(reserved_identifiers)
    
    with open(filename, 'r') as infile:
        
        lines = parse_first_pass(infile)
        #second pass, read constants
        for fields in lines:
            #fields = line.split('\t')
            if (len(fields) == 1):
                sides = fields[0].split('=')
                if (len(sides) != 2):
                    raise Exception("cannot parse line:" + str(line))
                else:
                    sides = [x.strip() for x in sides] #strip whitespaces
                    if (is_identifier(sides[0])): #and is_number(sides[1])):  ### actually, there's no reason the consts have to be numbers, they can be strings too
                        if (sides[0] in used_identifiers):
                            raise Exception("reassignment of constants not supported: " + str(line))
                        consts[sides[0]] = sides[1]  #TODO: should sides[1] be cast to a float?  I think not yet
                        used_identifiers.add(sides[0])
        
        #third pass, process the middle column
        for fields in lines:
            #fields = line.split('\t')
            if (len(fields) > 1): #if there are more than 1 field, it can only be a reaction line
                if (len(fields) == 3):
                    reaction_name = fields[0]
                    rxn = Reaction(substitute_constants(fields[1], consts)) ### these functions should do the error checking, so we only need to make sure that identifiers aren't being redeclared (basically, that means making sure the substitute_constants function worked, and that there is no reactant called "time")
                    multipled_identifiers = rxn.all_substrates().intersection(used_identifiers)
                    if (len(multipled_identifiers) > 0):
                        raise Exception("Illegal metabolite names: " + " ".join(multiplied_identifiers))
                    if (reaction_name in reactions):
                        raise Exception("reaction name used more than once: " + reaction_name)
                    reactions[reaction_name] = rxn
                else:
                    raise Exception("Cannot parse line: " + "\t".join(fields))
        
        # add substrate names to reserved words list 
        for rxn in reactions.values():
            substrate_names = substrate_names.union(rxn.all_substrates())
        used_identifiers = used_identifiers.union(substrate_names)
            
        #fourth pass, parse the rate law field

        for fields in lines:
            if (len(fields) == 3): #error checking for lines should have been done in the first three passes, so we can get right into it
                rate_string = fields[2]
                rate_string = substitute_constants(rate_string, consts) ## TODO: may want to integrate constants after sympy parsing
                
                #### at this point, rate_string should be a valid sympy expression that we can subs into ###
                # TODO: figure out a way to check if it's an 'algebraic expression' and not something else (I'm not super familiar
                # with sympy data types yet.)
                #http://stackoverflow.com/questions/7006626/how-to-calculate-expression-using-sympy-in-python
                exp = S(fields[2])
                symbols = set([str(symbol) for symbol in exp.atoms(Symbol)]) #I think this line will throw an exception if it's not an 'expression'
                #but I'm not 100% sure
                #exp.subs(consts) #TODO: use this line if consts are restricted to numbers

                new_symbols = symbols.difference(used_identifiers)
                rate_constants = rate_constants.union(new_symbols)
                reactions[fields[0]].add_rate_law(exp)
    #(self, reactions, rate_constants, initial_conditions = dict())
    return Model(reactions, rate_constants)
    

def is_identifier(string):
    """
        return true if the string is a valid python identifier (variable name),
        else false
    """
    out = False
    
    match = re.search('^' + IDENTIFIER_REGEX + '$', string)
    if match:
        out = True
    return out

def derivative_string(change_in, with_respect_to):
    out = "Derivative(" + change_in +"(" + with_respect_to + ")," + with_respect_to +")"
    return out

def function_string(name, of):
    out = name +"(" + of + ")"
    return out
    
def is_number(string):
    """
        return true if the string is a number,
        else false
    """
    out = False
    try: 
        float(string)
        out = True
    except error:
        pass
    
    return out
    
    
def parse_first_pass(file):
    """
        file is a file object
        first pass, remove comments, 
        split non-blank lines
        and store them in an array
    """
    lines = list()
    for line in file:
        line = line.split('#',1)[0] #get content in front of the comment symbol
        line = line.strip() #remove leading and trailing whitespace
        if not line == "":
            #no checking for valid syntax yet.
            #if it's not a blank line, go ahead and send it forward
            fields = line.split("\t")
            fields = [x.strip() for x in fields]
            lines.append(fields)
    return lines


def substitute_constants(string, constants):
    """
        string is a string
        constants is a dict where keys are strings and values are also strings
        
        everytime a key of constants is found as a complete word in string, replace it with
        the value of constants[that_key]
        TODO: THIS FUNCTION ONLY WORKS IF CONSTANTS ARE SEPARATED FROM NEIGHBORING TOKENS BY SPACES
                    THAT IS NOT CONSISTENT WITH OTHER PARTS OF THE PARSER, WHICH DON'T CARE!
                    that should be fixed... but it's not quite trivial
                    !!!Maybe easist just to restrict consts to numbers in rate expressions and use substitution with sympy
                    for more complicated substitutions, meta-model syntax can be used!!!
        TODO: 6/5/2013: yeah, this totally doesn't work, but it's not really a priority, so I'm ignoring it for now
    """
    words = string.split()
    for i in range(len(words)):
        if words[i] in constants:
            words[i] = constants[words[i]]
    
    return " ".join(words)
    
    
def parse_reaction_half(string):
    """
        string: a half reaction string
            <half_reaction> ::= <identifier> | <number> <indentifier> | <half_reaction> "+" <half_reaction>

            whitespace optional, but highly recommended for clarity and legibility
    """
    string = string.strip()
    terms = string.split('+')
    out = dict()
    if string != "": #return an empty dict if it's a blank string
        for term in terms:
            term = term.strip()
            #see if the term ends with an identifier
            match = re.search('(' + IDENTIFIER_REGEX + ')$', term)
            if match:
                reactant = match.group(1)
                
                # pull off the reactant name
                term = term[0:len(term)-len(reactant)]
                term = term.strip() #not really necessary because extra spaces don't inhibit casting it to a float
                stoich = 1.
                if (len(term) == 0):
                    pass
                    #do nothing, there is no coefficient, so just assume it's 1
                elif (is_number(term)):
                    #may be tempted to keep everything as ints for as long as possible, but it's gonna have to be a float eventually, so why not now?
                    stoich = float(term)
                else:
                    raise Exception('cannot parse reaction half: ' + string)
                
                if (not reactant in out):
                    out[reactant] = 0.
                out[reactant] += stoich
                
            else:
                #print
                raise Exception('cannot parse reaction half: ' + string)
    return out
    
def threshold_set(in_set, thresh, inclusive=True):
    out = set()
    op = operator.lt
    if inclusive:
        op = operator.le
    
    for x in in_set:
        if op(x,thresh):
            out.add(x)
    return out
    
def halve_string(string, splitters):
    """
        string: a string
        splitters: a list of strings
        splits the string in half based at any one of the strings in splitters
        if there are more than one possible breakpoints, raises an exception
    """
    
    splits = 0
    halves = list()
    for splitter in splitters:
        tmp_halves = string.split(splitter)
        if (len(tmp_halves) == 2):
            halves = tmp_halves
            #print tmp_halves
            splits += 1
        elif (len(tmp_halves) == 1):
            pass
            #no problem here, just means that a particular equals sign was not found
            #if no equals signs at all are found, it will be caught below
        else:
            
            raise Exception("reaction equation has multiple equals signs: " + string)
        
    if splits > 1:
        raise Exception("reaction equation has multiple equals signs: " + string)
    elif splits == 0:
        raise Exception("reaction equation has no equals signs: " + string)

    return halves
