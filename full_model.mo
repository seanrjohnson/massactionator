#reaction name	formula	rate expression
glycerol uptake	= glycerol	10**k0
denovo DAG synthesis	glycerol = DAG_1	(10**k1)*glycerol 
DAG_1 to PC_1	DAG_1 = PC_1	(10**k2)*DAG_1
PC_1 to DAG_1	PC_1 = DAG_1	(10**k3)*PC_1
PC_1 to PC_2	PC_1 = PC_2	(10**k4)*PC_1
PC_2 to PC_1	PC_2 = PC_1	(10**k5)*PC_2
PC_1 to DAG_2	PC_1 = DAG_2	(10**k6)*PC_1
DAG_2 to PC_1	DAG_2 = PC_1	(10**k7)*DAG_2
DAG_2 to DAG_3	DAG_2 = DAG_3	(10**k8)*DAG_2
DAG_3 to DAG_2	DAG_3 = DAG_2	(10**k9)*DAG_3
DAG_2 to TAG	DAG_2 = TAG_1	(10**k10)*DAG_2