import actionatorlib as al
import numpy as np
import pprint

def steady_state(model):
    model = model.copy()
        
    model.add_observable("TAG_model","TAG_1")
    model.add_observable("DAG_model","DAG_1 + DAG_2 + DAG_3")
    model.add_observable("PC_model","PC_1 + PC_2")
    #data_set = al.Dataset("data/Arabidopsis_glycerol_bates_with_100day_projection.txt") 
    data_set = al.Dataset("data/Arabidopsis_glycerol_bates.txt")
    #model.set_rate_constants(consts)
    
    times = np.linspace(0,1000,1000)
    #times[-1] = (max(data_set.times()))
    #times = data_set.times()
    (obs, met) = model.integrate(times)
    last = {x:met[x][-1] for x in met}
    pprint.pprint(last)
    #print obs
    #print data_set.data()
    #print data
    #al.graph_multiple((met, times),(obs, times), (data_set.data(), data_set.times()))
    al.graph_multiple((obs, times), (data_set.data(), data_set.times()))

def pulse_chase(model):
    model = model.copy()
    
    #data_set = al.Dataset("data/Arabidopsis_glycerol_Lu_chase_normalized_limit.txt")
    #data_set = al.Dataset("data/Arabidopsis_glycerol_Lu_chase_normalized.txt")
    data_set = al.Dataset("data/Arabidopsis_rod1_glycerol_Lu_chase_normalized.txt")
    
    model.add_observable("TAG_p_model","100 * TAG_1/(TAG_1 + DAG_1 + DAG_2 + DAG_3 + PC_1 + PC_2 + 0.0000000001)") #0.0000000001 is just a tiny number to avoid divide by zero errors
    model.add_observable("DAG_p_model","100 * (DAG_1 + DAG_2 + DAG_3)/(TAG_1 + DAG_1 + DAG_2 + DAG_3 + PC_1 + PC_2 + 0.0000000001)")
    model.add_observable("PC_p_model","100 * (PC_1 + PC_2)/(TAG_1 + DAG_1 + DAG_2 + DAG_3 + PC_1 + PC_2 + 0.0000000001)")
    model.add_trigger(15,'k0',0)
    
    times = np.linspace(0,200,200)
    
    #times[-1] = (max(data_set.times()))
    #times = data_set.times()
    (obs, met) = model.integrate(times)
    #print obs
    #print data_set.data()
    #print data
    al.graph_multiple((obs, times), (data_set.data(), data_set.times()))
    

if __name__ == "__main__":
    #consts = {'k10': 0.29196444761555379, 'k3': 1.0012329748877349, 'k2': 0.83366335999777463, 'k1': 16.309503668441931, 'k0': 137.39696435645061, 'k7': 0.23946993241491399, 'k6':0.23001177072822607, 'k5': 0.0060903295672112921, 'k4': 0.24348692819250234, 'k9': 0.056700152494463724, 'k8': 1.2093798980963646}
    #consts = {'k10': 0.35473354195864587, 'k3': 1.5012395106285235, 'k2': 0.54296740839947932, 'k1': 0.064353479613896633, 'k0': 137.53475574107719, 'k7': 1.7955083400924241, 'k6': 0.081564054707721406, 'k5': 0.53218323626138941, 'k4': 1.7882599528014624, 'k9': 0.74869077047253374, 'k8': 1.5075970164446328}
    #consts = {'k10': 0.41862024298393669, 'k3': 0.80981497548680659, 'k2': 1.2892659563517821, 'k1': 0.15885992730776069, 'k0': 80.684565370496813, 'k7': 0.26521570351875545, 'k6':0.28381965130600717, 'k5': 0.001483893994371342, 'k4': 0.14089120917161976, 'k9': 0.0082431131365202706, 'k8': 1.2470092580592669}
    #consts = {'k10': 0.017467998497802801, 'k3': 5.5848779191902622, 'k2': 4.0269180234427511, 'k1': 3.618263845060572, 'k0': 106.7904805780564, 'k7': 0.45167804900514624, 'k6': 4.8634247144943545, 'k5': 0.2689623898011213, 'k4': 4.9042096329253049, 'k9': 0.26221366789591272, 'k8': 0.0036293264704470338}
    #consts = {"k0": 143.548,"k1": 0.074827,"k2": 0.53183,"k3": 0.0,"k4": 0.2375,"k5": 0.013433,"k6": 0.228894,"k7": 0.233858,"k8": 1.20513,"k9": 0.058923,"k10": 0.291628}
    consts = {"k1":0.0748266,"k2":1.03979,"k8":0.303403,"k7":2.05166,"k10":0.273605,"k9":0.00240532,"k0":137.52,"k3":0.604603,"k6":0.216647,"k4":0.0458241,"k5":0.00298743} #from copasi match to both datasets with long time points
    #consts={'k1':0.0748266,'k2':0.042067223,'k8':0.303403,'k7':0,'k10':0.273605,'k9':0.00240532,'k0':137.52,'k3':0,'k6':0.02549202,'k4':0.0458241,'k5':0.00298743} #like above, but with calculated PDCT activities removed
    model = al.read_model("full_model.mo")
    model.set_rate_constants(consts)
    
    steady_state(model)
    #pulse_chase(model)
    